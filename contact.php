<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>PHP Mailer with GMAIL | CONTACT</title>

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


    <style>
        body{
            background-color: #ddd;
        }
        .card{
            background-color: #fff;
            border-radius: 0px;
            margin-top: 10%;
        }
        .form-group span{
            color: red;
            font-size: 16px;
            font-weight: bold;
        }
        .form-control{
            border-radius: 0px;
            transition: 0.3s all ease-in-out;
        }
        .form-control:focus {
            color: #495057;
            background-color: #fff;
            border-color: #3a3a3a;
            outline: 0;
            box-shadow: none;
            transition: 0.3s all ease-in-out;
        }

        .btn-outline-dark{
            border-radius: 0px;
            outline: none !important;
        }

        .btn-outline-dark:focus,
        .btn-outline-dark:active,
        .btn-outline-dark:hover{
            border-radius: 0px;
            box-shadow: none !important;
            outline: none !important;
        }
    </style>
  </head>
  <body>
    <section class="contact-form">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <form action="mail/config/data.php" method="post">
                        <div class="card">
                            <div class="card-header text-center">
                                <h4>Contact Form</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="first_name">First Name <span>*</span></label>
                                            <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Jhon Doe" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="last_name">Last Name <span>*</span></label>
                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Jhon Doe" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email Address <span>*</span></label>
                                            <input type="email" class="form-control" id="email" name="email" placeholder="something@gmail.com" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="number">Mobile Number <span>*</span></label>
                                            <input type="text" class="form-control" id="number" name="number" placeholder="+8801234567898" required>
                                        </div>
                                    </div>

                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="message">Message <span>*</span></label>
                                            <textarea class="form-control" id="message" name="message" rows="3"  placeholder="Your Message here." required></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer text-right">
                                <button class="btn btn-outline-dark btn-sm" type="submit" name="submit" value='contact'>SUBMIT</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>